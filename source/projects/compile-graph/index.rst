compile-graph
=============

`Code Repo <https://github.com/bnetbutter/compile-graph>`_

This is a quick project I made that graphs the dependency of C/C++ source code
during preprocessing, compilation, and linking. My C++ professor explained the steps
but didn't really have us execute the commands. This utility guides students through
each step of the process in a simple source repo.

.. image:: _static/compile-graph-demo.gif